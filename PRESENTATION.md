# DDD Presentation Slides content
## Intro
### Who am I
### What are we going to talk about
### Out of school

## TDD
### What is a test
In software development, testing is way to ensure the quality of your product. 

They allow to check that the software:

- Provides the expected output.
- Behaves the way it’s expected to.
- Works in its environment.

### Different kind of tests
Example using a car. Macro to micro

#### End-to-ends
#### Functional
#### Integration
#### Unit

### Unit Test: small demo (talk about npm, mocha, chai)
### EXAMPLE: Basic
### What is TDD?
### EXAMPLE: Show function documentation
### Red Green Refactor

## Coding Dojo
### What is it?
### How does it work?
### It’s a safe place
### Multiple format

## Conclusion


## Feedback session 1 w/ Anwesha

- Mention the tech stack (what editor, what techno...).
- Explain more deeply what mocha and chai are.
- Slide 15: Ditch the description and just put the "code extract". Add the Red green refactor cycle.
- Remove the clean code section.
- Work on the whole test explanation sections.
- Coding dojo etiquette, spends more time on it.
- Don't say "Everybody knows JS" --> Most people know.
