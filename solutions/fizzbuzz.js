
const FIZZ = 'Fizz'
const BUZZ = 'Buzz'

function fizzbuzz(number) {
    const isFizz = number % 3 === 0;
    const isBuzz = number % 5 === 0;

    if (isFizz && isBuzz) {
        return FIZZ + BUZZ
    }

    if (isFizz) {
        return FIZZ
    }

    if (isBuzz) {
        return BUZZ
    }

    return number;
}

module.exports = fizzbuzz