const assert = require('chai').expect

it('should equal 5 when adding 2 and 3', () => {
    assert(2+3).equal(5)
})

const ZERO = "zero"
const POSITIVE = "positive"
const NEGATIVE = "negative"

function foo(number) {
    if(number< 0){
        return NEGATIVE
    }
    if(number> 0){
        return POSITIVE
    }
    return ZERO;
}

describe('foo', () => {
    it('should return zero when given 0', () => {
        assert(foo(0)).equal("zero")
    })

    it('should return "positive"', () => {
        assert(foo(1)).equal("positive")
    })

    it('should return "negative"', () => {
        assert(foo(-1)).equal("negative")
    })
})