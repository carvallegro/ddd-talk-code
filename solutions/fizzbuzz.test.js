const {suite, test} = require('mocha')
const {assert} = require('chai')

const fizzbuzz = require('./fizzbuzz')

suite('describe the fizzbuss', () => {
    test('it should return 1 when given 1', () => {
        assert.equal(1, fizzbuzz(1))
    })

    test('it should return 2 when given 2', () => {
        assert.equal(2, fizzbuzz(2))
    })

    test('it should return Fizz when given 3', () => {
        assert.equal('Fizz', fizzbuzz(3))
    })

    test('it should return Buzz when given 5', () => {
        assert.equal('Buzz', fizzbuzz(5))
    })

    test('it should return Fizz when given any multiple of 3', () => {
        assert.equal('Fizz', fizzbuzz(12))
    })
    
    test('it should return Buzz when given any multiple of 5', () => {
        assert.equal('Buzz', fizzbuzz(25))
    })
    
    test('it should return FizzBuzz when given any multiple of 3 AND 5', () => {
        assert.equal('FizzBuzz', fizzbuzz(15))
    })
})