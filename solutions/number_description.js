
const ZERO = 'zero'
const POSITIVE = 'positive'
const NEGATIVE = 'negative'
const AND_ODD = ' and odd'
/**
 * This function takes an integer and returns a string describing it.
 * If the parameter is 0, the result will be "zero".
 * If the parameter is positive, it will return "positive", and "negative" otherwise.
 * If the parameter is odd, it will add "and odd" to the result.
 * 
 * Examples:
 *   business_function(0) returns "zero"
 *   business_function(10) returns "positive"
 *   business_function(-4) returns "negative"
 *   business_function(-5) returns "negative and odd"
 * 
 * @param param the integer to describe
 */
// TODO: rename describeNunber(number)
function business_function(param){
    let result = ZERO;
    if(param > 0) {
        result = POSITIVE
    }

    if(param < 0) {
        result = NEGATIVE
    }

    if(param % 2 != 0) {
        result += AND_ODD
    }

    return result;
}

module.exports = business_function