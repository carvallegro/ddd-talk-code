const {suite, test} = require('mocha')
const {assert} = require('chai')

const business_function = require('./number_description')

suite('#business_function()', () => {
    test('it returns "zero" when given 0', () => {
        assert.equal(business_function(0), "zero")
    })

    test('it returns "positive" when given 2', () => {
        assert.equal(business_function(2), "positive")
    })

    test('it returns "negative" when given -4', () => {
        assert.equal(business_function(-4), "negative")
    })

    test('it returns "positve and odd" when given 1', () => {
        assert.equal(business_function(1), "positive and odd")
    })

    test('it returns "negative and odd" when given -13', () => {
        assert.equal(business_function(-13), 'negative and odd')
    })
})