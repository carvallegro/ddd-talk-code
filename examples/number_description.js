
/**
 * This function takes an integer and returns a string describing it.
 * If the parameter is 0, the result will be "zero".
 * If the parameter is positive, it will return "positive", and "negative" otherwise.
 * If the parameter is odd, it will add "and odd" to the result.
 * 
 * Examples:
 *   business_function(0) returns "zero"
 *   business_function(10) returns "positive"
 *   business_function(-4) returns "negative"
 *   business_function(-5) returns "negative and odd"
 * 
 * @param param the integer to describe
 */
function business_function(param){
    // TODO: complete
    return null;
}

module.exports = business_function